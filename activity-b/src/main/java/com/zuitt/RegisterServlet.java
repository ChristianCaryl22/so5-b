package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6116303543789795413L;

	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String app = req.getParameter("app");
		String dateOfBirth = req.getParameter("date_of_birth");
		String position = req.getParameter("position");
		String description = req.getParameter("description");
		
	//Stores all the data from the form into the session
		HttpSession session = req.getSession();
		session.setAttribute("firstname", firstname);
		session.setAttribute("lastname", lastname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("app", app);
		session.setAttribute("date_of_birth", dateOfBirth);
		session.setAttribute("position", position);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been destroyed.");
	}
}
