<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration</title>
</head>
<body>

	<%
		String app = session.getAttribute("app").toString();
	
		if(app.equals("friends")){
			app="friends";
		} else if (app.equals("social_media")){
			app="social media";
		} else {
			app="others";
		}
		
		String birthdate = session.getAttribute("date_of_birth").toString();
		birthdate = birthdate.replace("T", " - ");
	%>
	
	<h1>Registration Confirmation</h1>
	<p>First Name: <%=session.getAttribute("firstname") %></p>
	<p>Last Name: <%=session.getAttribute("lastname") %></p>
	<p>Phone: <%=session.getAttribute("phone") %></p>
	<p>Email: <%=session.getAttribute("email") %></p>
	<p>App Discovery: <%= app %></p>
	<p>Date of Birth: <%= birthdate %></p>
	<p>User Type: <%=session.getAttribute("position") %></p>
	<p>Description: <%=session.getAttribute("description") %></p>
	
	<form action="login" method="post">
		<input type="submit">
	</form>
	<br>
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>
	
		
</body>
</html>
